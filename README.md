Monitoramento com Prometheus e Grafana
=====

Vamos mostrar agora o monitoramento de uma aplicação Java com Prometheus e Grafana!


Para esta seção, vamos criar o namespace "monitoring", com o seguinte comando:

```
# kubectl create ns monitoring
```

Resultado: 
`namespace/monitoring created`

Executar na seguinte ordem:


# 1 - Criar o projeto Java que usaremos como exemplo:

```
# kubectl apply -f cars-api-deployment.yaml -n monitoring
# kubectl apply -f cars-api-service.yaml -n monitoring
```

Resultado: 

`deployment.apps/cars-api created`
`service/cars-api created`

Teste o serviço:

http://IP_EXTERNO:NODE_PORT/cars/home


# 2 - Instalação do Prometheus e Grafana (Helm chart)

```
# helm install --name monitoring stable/prometheus-operator --namespace monitoring --set prometheus.prometheusSpec.serviceMonitorNamespaceSelector.any=true
```

A label serviceMonitorNamespaceSelector.any=true signifca que estamos habilitando a descoberta de ServiceMonitor em todos os namespaces.

Resultado:

```
NAME:   monitoring
LAST DEPLOYED: Thu May 23 14:52:02 2019
NAMESPACE: monitoring
STATUS: DEPLOYED

RESOURCES:
==> v1/Secret
NAME                                                  TYPE    DATA  AGE
monitoring-grafana                                    Opaque  3     3s
alertmanager-monitoring-prometheus-oper-alertmanager  Opaque  1     3s

==> v1beta1/ClusterRole
NAME                                     AGE
monitoring-kube-state-metrics            3s
psp-monitoring-prometheus-node-exporter  3s

==> v1/Alertmanager
NAME                                     AGE
monitoring-prometheus-oper-alertmanager  2s

==> v1/PrometheusRule
NAME                                                             AGE
monitoring-prometheus-oper-alertmanager.rules                    2s
monitoring-prometheus-oper-etcd                                  2s
monitoring-prometheus-oper-general.rules                         2s
monitoring-prometheus-oper-k8s.rules                             2s
monitoring-prometheus-oper-kube-apiserver.rules                  2s
monitoring-prometheus-oper-kube-prometheus-node-alerting.rules   2s
monitoring-prometheus-oper-kube-prometheus-node-recording.rules  2s
monitoring-prometheus-oper-kube-scheduler.rules                  2s
monitoring-prometheus-oper-kubernetes-absent                     2s
monitoring-prometheus-oper-kubernetes-apps                       2s
monitoring-prometheus-oper-kubernetes-resources                  2s
monitoring-prometheus-oper-kubernetes-storage                    2s
monitoring-prometheus-oper-kubernetes-system                     2s
monitoring-prometheus-oper-node-network                          2s
monitoring-prometheus-oper-node-time                             2s
monitoring-prometheus-oper-node.rules                            2s
monitoring-prometheus-oper-prometheus-operator                   2s
monitoring-prometheus-oper-prometheus.rules                      2s

==> v1/ConfigMap
NAME                                                          DATA  AGE
monitoring-grafana-config-dashboards                          1     3s
monitoring-grafana                                            1     3s
monitoring-grafana-test                                       1     3s
monitoring-prometheus-oper-grafana-datasource                 1     3s
monitoring-prometheus-oper-etcd                               1     3s
monitoring-prometheus-oper-k8s-cluster-rsrc-use               1     3s
monitoring-prometheus-oper-k8s-coredns                        1     3s
monitoring-prometheus-oper-k8s-node-rsrc-use                  1     3s
monitoring-prometheus-oper-k8s-resources-cluster              1     3s
monitoring-prometheus-oper-k8s-resources-namespace            1     3s
monitoring-prometheus-oper-k8s-resources-pod                  1     3s
monitoring-prometheus-oper-k8s-resources-workload             1     3s
monitoring-prometheus-oper-k8s-resources-workloads-namespace  1     3s
monitoring-prometheus-oper-nodes                              1     3s
monitoring-prometheus-oper-persistentvolumesusage             1     3s
monitoring-prometheus-oper-pods                               1     3s
monitoring-prometheus-oper-statefulset                        1     3s

==> v1/Service
NAME                                                TYPE       CLUSTER-IP      EXTERNAL-IP  PORT(S)    AGE
monitoring-grafana                                  ClusterIP  10.104.175.197  <none>       80/TCP     2s
monitoring-kube-state-metrics                       ClusterIP  10.109.54.122   <none>       8080/TCP   2s
monitoring-prometheus-node-exporter                 ClusterIP  10.100.145.29   <none>       9100/TCP   2s
monitoring-prometheus-oper-alertmanager             ClusterIP  10.107.51.7     <none>       9093/TCP   2s
monitoring-prometheus-oper-coredns                  ClusterIP  None            <none>       9153/TCP   2s
monitoring-prometheus-oper-kube-controller-manager  ClusterIP  None            <none>       10252/TCP  2s
monitoring-prometheus-oper-kube-etcd                ClusterIP  None            <none>       2379/TCP   2s
monitoring-prometheus-oper-kube-scheduler           ClusterIP  None            <none>       10251/TCP  2s
monitoring-prometheus-oper-operator                 ClusterIP  10.109.65.123   <none>       8080/TCP   2s
monitoring-prometheus-oper-prometheus               ClusterIP  10.101.91.71    <none>       9090/TCP   2s

==> v1beta1/DaemonSet
NAME                                 DESIRED  CURRENT  READY  UP-TO-DATE  AVAILABLE  NODE SELECTOR  AGE
monitoring-prometheus-node-exporter  3        3        0      3           0          <none>         2s

==> v1beta1/Role
NAME                AGE
monitoring-grafana  3s

==> v1/RoleBinding
NAME                                          AGE
monitoring-grafana-test                       2s
monitoring-prometheus-oper-prometheus-config  2s
monitoring-prometheus-oper-prometheus         2s
monitoring-prometheus-oper-prometheus         2s

==> v1beta2/Deployment
NAME                DESIRED  CURRENT  UP-TO-DATE  AVAILABLE  AGE
monitoring-grafana  1        1        1           0          2s

==> v1/Deployment
NAME                                 DESIRED  CURRENT  UP-TO-DATE  AVAILABLE  AGE
monitoring-prometheus-oper-operator  1        1        1           0          2s

==> v1/ServiceMonitor
NAME                                                AGE
monitoring-prometheus-oper-alertmanager             2s
monitoring-prometheus-oper-coredns                  2s
monitoring-prometheus-oper-apiserver                2s
monitoring-prometheus-oper-kube-controller-manager  2s
monitoring-prometheus-oper-kube-etcd                2s
monitoring-prometheus-oper-kube-scheduler           2s
monitoring-prometheus-oper-kube-state-metrics       2s
monitoring-prometheus-oper-kubelet                  2s
monitoring-prometheus-oper-node-exporter            2s
monitoring-prometheus-oper-grafana                  2s
monitoring-prometheus-oper-operator                 1s
monitoring-prometheus-oper-prometheus               1s

==> v1/ServiceAccount
NAME                                     SECRETS  AGE
monitoring-grafana                       1        3s
monitoring-grafana-test                  1        3s
monitoring-kube-state-metrics            1        3s
monitoring-prometheus-node-exporter      1        3s
monitoring-prometheus-oper-alertmanager  1        3s
monitoring-prometheus-oper-operator      1        3s
monitoring-prometheus-oper-prometheus    1        3s

==> v1/ClusterRoleBinding
NAME                                       AGE
monitoring-grafana-clusterrolebinding      3s
psp-monitoring-kube-state-metrics          3s
monitoring-prometheus-oper-alertmanager    3s
monitoring-prometheus-oper-operator        3s
monitoring-prometheus-oper-operator-psp    3s
monitoring-prometheus-oper-prometheus      3s
monitoring-prometheus-oper-prometheus-psp  3s

==> v1beta1/ClusterRoleBinding
NAME                                     AGE
monitoring-kube-state-metrics            3s
psp-monitoring-prometheus-node-exporter  3s

==> v1/Pod(related)
NAME                                                 READY  STATUS             RESTARTS  AGE
monitoring-prometheus-node-exporter-fnq6s            0/1    ContainerCreating  0         2s
monitoring-prometheus-node-exporter-gbpb4            0/1    ContainerCreating  0         2s
monitoring-prometheus-node-exporter-z6szb            0/1    ContainerCreating  0         2s
monitoring-grafana-b7b7568bd-w7jm7                   0/2    Init:0/1           0         2s
monitoring-kube-state-metrics-5795c486d-hm2nw        0/1    ContainerCreating  0         2s
monitoring-prometheus-oper-operator-d596f5c94-9gcpp  0/1    ContainerCreating  0         2s

==> v1beta1/RoleBinding
NAME                AGE
monitoring-grafana  2s

==> v1beta1/Deployment
NAME                           DESIRED  CURRENT  UP-TO-DATE  AVAILABLE  AGE
monitoring-kube-state-metrics  1        1        1           0          2s

==> v1/Prometheus
NAME                                   AGE
monitoring-prometheus-oper-prometheus  2s

==> v1beta1/PodSecurityPolicy
NAME                                     PRIV   CAPS      SELINUX           RUNASUSER  FSGROUP    SUPGROUP  READONLYROOTFS  VOLUMES
monitoring-grafana                       false  RunAsAny  RunAsAny          RunAsAny   RunAsAny   false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim
monitoring-grafana-test                  false  RunAsAny  RunAsAny          RunAsAny   RunAsAny   false     configMap,downwardAPI,emptyDir,projected,secret
monitoring-kube-state-metrics            false  RunAsAny  MustRunAsNonRoot  MustRunAs  MustRunAs  false     secret
monitoring-prometheus-node-exporter      false  RunAsAny  RunAsAny          MustRunAs  MustRunAs  false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim,hostPath
monitoring-prometheus-oper-alertmanager  false  RunAsAny  RunAsAny          MustRunAs  MustRunAs  false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim
monitoring-prometheus-oper-operator      false  RunAsAny  RunAsAny          MustRunAs  MustRunAs  false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim
monitoring-prometheus-oper-prometheus    false  RunAsAny  RunAsAny          MustRunAs  MustRunAs  false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim

==> v1/ClusterRole
NAME                                       AGE
monitoring-grafana-clusterrole             3s
psp-monitoring-kube-state-metrics          3s
monitoring-prometheus-oper-alertmanager    3s
monitoring-prometheus-oper-operator        3s
monitoring-prometheus-oper-operator-psp    3s
monitoring-prometheus-oper-prometheus      3s
monitoring-prometheus-oper-prometheus-psp  3s

==> v1/Role
NAME                                          AGE
monitoring-grafana-test                       2s
monitoring-prometheus-oper-prometheus-config  2s
monitoring-prometheus-oper-prometheus         2s
monitoring-prometheus-oper-prometheus         2s


NOTES:
The Prometheus Operator has been installed. Check its status by running:
  kubectl --namespace monitoring get pods -l "release=monitoring"

Visit https://github.com/coreos/prometheus-operator for instructions on how
to create & configure Alertmanager and Prometheus instances using the Operator.
```


Depois de alguns instantes, faça:

```
# kubectl get all -n monitoring
```


Resultado:


```
NAME                                                         READY   STATUS    RESTARTS   AGE
pod/alertmanager-monitoring-prometheus-oper-alertmanager-0   2/2     Running   0          16m
pod/monitoring-grafana-b7b7568bd-w7jm7                       2/2     Running   0          16m
pod/monitoring-kube-state-metrics-5795c486d-hm2nw            1/1     Running   0          16m
pod/monitoring-prometheus-node-exporter-fnq6s                1/1     Running   0          16m
pod/monitoring-prometheus-node-exporter-gbpb4                1/1     Running   0          16m
pod/monitoring-prometheus-node-exporter-z6szb                1/1     Running   0          16m
pod/monitoring-prometheus-oper-operator-d596f5c94-9gcpp      1/1     Running   0          16m
pod/prometheus-monitoring-prometheus-oper-prometheus-0       3/3     Running   1          16m

NAME                                              TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)             AGE
service/alertmanager-operated                     ClusterIP   None             <none>        9093/TCP,6783/TCP   16m
service/monitoring-grafana                        ClusterIP   10.104.175.197   <none>        80/TCP              16m
service/monitoring-kube-state-metrics             ClusterIP   10.109.54.122    <none>        8080/TCP            16m
service/monitoring-prometheus-node-exporter       ClusterIP   10.100.145.29    <none>        9100/TCP            16m
service/monitoring-prometheus-oper-alertmanager   ClusterIP   10.107.51.7      <none>        9093/TCP            16m
service/monitoring-prometheus-oper-operator       ClusterIP   10.109.65.123    <none>        8080/TCP            16m
service/monitoring-prometheus-oper-prometheus     ClusterIP   10.101.91.71     <none>        9090/TCP            16m
service/prometheus-operated                       ClusterIP   None             <none>        9090/TCP            16m

NAME                                                 DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
daemonset.apps/monitoring-prometheus-node-exporter   3         3         3       3            3           <none>          16m

NAME                                                  READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/monitoring-grafana                    1/1     1            1           16m
deployment.apps/monitoring-kube-state-metrics         1/1     1            1           16m
deployment.apps/monitoring-prometheus-oper-operator   1/1     1            1           16m

NAME                                                            DESIRED   CURRENT   READY   AGE
replicaset.apps/monitoring-grafana-b7b7568bd                    1         1         1       16m
replicaset.apps/monitoring-kube-state-metrics-5795c486d         1         1         1       16m
replicaset.apps/monitoring-prometheus-oper-operator-d596f5c94   1         1         1       16m

NAME                                                                    READY   AGE
statefulset.apps/alertmanager-monitoring-prometheus-oper-alertmanager   1/1     16m
statefulset.apps/prometheus-monitoring-prometheus-oper-prometheus       1/1     16m
```

# 3 - Altere os serviços para NodePort:


Para que possamos acessar os serviços do Prometheus e Grafana, após o deploy do Prometheus Operator através do helm, precisamos expor estes serviços no nosso cluster.

Por padrão, todos os serviços são expostos através de ClusterIP, desta forma são acessíveis apenas dentro do cluster.

Como estamos trabalhando em um cluster local, iremos expor os serviços através de NodePort, que disponibilizará uma porta em cada nó do cluster para que possamos acessar os serviços. 
Para ambientes de produção, sugiro que outras abordagens sejam utilizadas, como Ingress, por exemplo.


- Alterar Service type do Prometheus:

```
kubectl edit svc monitoring-prometheus-oper-prometheus -n monitoring
```
Salve e feche o arquivo:

`service/monitoring-prometheus-oper-prometheus edited`

- Alterar Service type do Grafana:

```
kubectl edit svc monitoring-grafana -n monitoring
```

Salve e feche o arquivo:

`service/service/monitoring-grafana edited edited`



```

# kubectl get service -n monitoring

NAME                                      TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)             AGE
alertmanager-operated                     ClusterIP   None             <none>        9093/TCP,6783/TCP   148m
monitoring-grafana                        NodePort    10.104.175.197   <none>        80:5713/TCP         148m
monitoring-kube-state-metrics             ClusterIP   10.109.54.122    <none>        8080/TCP            148m
monitoring-prometheus-node-exporter       ClusterIP   10.100.145.29    <none>        9100/TCP            148m
monitoring-prometheus-oper-alertmanager   ClusterIP   10.107.51.7      <none>        9093/TCP            148m
monitoring-prometheus-oper-operator       ClusterIP   10.109.65.123    <none>        8080/TCP            148m
monitoring-prometheus-oper-prometheus     NodePort    10.101.91.71     <none>        9090:14343/TCP      148m
prometheus-operated                       ClusterIP   None             <none>        9090/TCP            148m
```


Acesse o Grafana e Prometheus  no Browser:

http://IP_EXTERNO:NODE_PORT

Usuário e senha do Grafana:

```
admin
prom-operator
```

# 4 - ServiceMonitor

Especifica declarativamente como os grupos de serviços devem ser monitorados. 
Ele abstrai a configuração dos targets do Prometheus, facilitando a auto-descoberta dos serviços.

![Service Monitor](imagens/serviceMonitor.png)


Crie o service monitor:

```
kubectl apply -f serviceMonitor.yaml -n monitoring
```

`servicemonitor.monitoring.coreos.com/cars-api created`


Faça uma requisição ao serviços "cars-api" e depois acesse o prometheus:

http://IP_EXTERNO:NODE_PORT/targets

![Prometheus](imagens/prometheus.png)




https://github.com/helm/charts/tree/master/stable/prometheus-operator


# 4 - Visualização de métricas no Grafana

Agora vamos visualizar as métricas do serviço, dentro do Grafana.

Acesse a ferramenta e faça o login: http://IP_EXTERNO:NODE_PORT/login


Para visualizar as métricas da aplicação no Grafana, vamos utilizar um Dashboard do Micrometer, disponibilizado para a comunidade.
Ele já possui diversos dashboards criados para aplicações instrumentadas pelo Micrometer.

![Micrometer](imagens/micrometer.png)
https://grafana.com/dashboards/4701

Vamos importar o Dashboard para o Grafana:

![Load](imagens/import1.png)

![Import](imagens/import2.png)


Resultado do dashboard:

![Resultado](imagens/cars-grafana.png)






